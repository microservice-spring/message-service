package br.com.brunobrasilweb.messageservice.data.converter;

import br.com.brunobrasilweb.messageservice.data.dto.MessageRequestDTO;
import br.com.brunobrasilweb.messageservice.data.model.Message;
import org.springframework.stereotype.Component;

@Component
public class MessageConverter {

    public Message toModel(MessageRequestDTO messageRequestDTO) {
        return Message.builder()
                .text(messageRequestDTO.getText())
                .build();
    }

}
