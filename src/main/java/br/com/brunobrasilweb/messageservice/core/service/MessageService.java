package br.com.brunobrasilweb.messageservice.core.service;

import br.com.brunobrasilweb.messageservice.api.stream.MessageProducer;
import br.com.brunobrasilweb.messageservice.data.converter.MessageConverter;
import br.com.brunobrasilweb.messageservice.data.dto.MessageRequestDTO;
import br.com.brunobrasilweb.messageservice.data.model.Message;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@Slf4j
public class MessageService {

    private final MessageProducer producer;
    private final MessageConverter converter;

    public void send(MessageRequestDTO messageRequestDTO) {
        Message message = converter.toModel(messageRequestDTO);

        producer.send(message);

        log.info("Message sent event.");
    }
}
