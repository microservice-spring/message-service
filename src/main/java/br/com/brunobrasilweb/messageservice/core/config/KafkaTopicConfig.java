package br.com.brunobrasilweb.messageservice.core.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static br.com.brunobrasilweb.kafkacore.KafkaConfig.newTopic;

@Configuration
public class KafkaTopicConfig {

    public static final String SEND_MESSAGE_TELEGRAM_TOPIC = "SEND_MESSAGE_TELEGRAM";
    public static final String SEND_MESSAGE_TELEGRAM_RETURN_TOPIC = "SEND_MESSAGE_TELEGRAM_RETURN";

    @Bean
    public NewTopic sendMessageTelegramTopic() {
        return newTopic(SEND_MESSAGE_TELEGRAM_TOPIC);
    }

    @Bean
    public NewTopic sendMessageTelegramReturnTopic() {
        return newTopic(SEND_MESSAGE_TELEGRAM_RETURN_TOPIC);
    }

}
