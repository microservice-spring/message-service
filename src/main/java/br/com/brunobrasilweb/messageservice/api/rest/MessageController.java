package br.com.brunobrasilweb.messageservice.api.rest;

import br.com.brunobrasilweb.messageservice.core.service.MessageService;
import br.com.brunobrasilweb.messageservice.data.dto.MessageRequestDTO;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/message")
@AllArgsConstructor
public class MessageController {

    private final MessageService service;

    @PostMapping
    public void send(@RequestBody @Valid MessageRequestDTO message) {
        service.send(message);
    }

}
