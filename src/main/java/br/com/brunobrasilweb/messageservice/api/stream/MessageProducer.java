package br.com.brunobrasilweb.messageservice.api.stream;

import br.com.brunobrasilweb.kafkacore.KafkaAbstractProducer;
import br.com.brunobrasilweb.messageservice.data.model.Message;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import static br.com.brunobrasilweb.messageservice.core.config.KafkaTopicConfig.SEND_MESSAGE_TELEGRAM_TOPIC;

@Component
@Slf4j
public class MessageProducer extends KafkaAbstractProducer {

    public void send(Message event) {
        sendMessage(SEND_MESSAGE_TELEGRAM_TOPIC, event);

        log.info("Event sent. Topic: [{}], Event: [{}].", SEND_MESSAGE_TELEGRAM_TOPIC, event);
    }

}
