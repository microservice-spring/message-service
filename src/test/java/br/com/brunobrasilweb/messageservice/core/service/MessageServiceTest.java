package br.com.brunobrasilweb.messageservice.core.service;

import br.com.brunobrasilweb.messageservice.api.stream.MessageProducer;
import br.com.brunobrasilweb.messageservice.data.converter.MessageConverter;
import br.com.brunobrasilweb.messageservice.data.dto.MessageRequestDTO;
import br.com.brunobrasilweb.messageservice.data.model.Message;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MessageServiceTest {

    @Mock
    MessageProducer producer;

    @Mock
    MessageConverter converter;

    @InjectMocks
    MessageService service;

    private static final String TEXT = "Oi mundo.";

    @Test
    void givenRequestDTOCorrect_whenSend_thenSuccess() throws Exception {
        MessageRequestDTO requestDTO = MessageRequestDTO.builder()
                .text(TEXT)
                .build();

        Message message = Message.builder()
                .text(TEXT)
                .build();

        when(converter.toModel(requestDTO)).thenReturn(message);

        service.send(requestDTO);

        verify(converter).toModel(requestDTO);
        verify(producer).send(message);
    }

}
