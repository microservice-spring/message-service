package br.com.brunobrasilweb.messageservice.api.rest;

import br.com.brunobrasilweb.messageservice.core.service.MessageService;
import br.com.brunobrasilweb.messageservice.data.dto.MessageRequestDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
public class MessageControllerTest {

    @Mock
    private MessageService service;

    @InjectMocks
    private MessageController controller;

    private MockMvc mockMvc;

    private static final ObjectMapper mapper = new ObjectMapper().findAndRegisterModules().disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

    private static final String ENDPOINT = "/message";
    private static final String TEXT = "Oi mundo.";

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(controller)
                .build();
    }

    @Test
    void givenRequestWithText_whenCallEndpoint_thenProcessOk() throws Exception {
        MessageRequestDTO requestDTO = MessageRequestDTO.builder()
                .text(TEXT)
                .build();

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(ENDPOINT)
                .contentType(APPLICATION_JSON)
                .content(mapper.writeValueAsString(requestDTO));

        mockMvc.perform(request)
                .andExpect(status().isOk());

        verify(service).send(requestDTO);
    }

    @Test
    void givenRequestWithTextNull_whenCallEndpoint_thenValidate() throws Exception {
        MessageRequestDTO requestDTO = MessageRequestDTO.builder()
                .text(null)
                .build();

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(ENDPOINT)
                .contentType(APPLICATION_JSON)
                .content(mapper.writeValueAsString(requestDTO));

        mockMvc.perform(request)
                .andExpect(status().isBadRequest());

        verifyNoInteractions(service);
    }

}
